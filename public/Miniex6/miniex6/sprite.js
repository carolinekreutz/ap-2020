class Star {

  constructor() {
    this.x = random(-width, width);
    this.y = random(-height, height);
    this.z = random(width);
    this.pz = this.z;
  }

  update() {
    this.z = this.z - speed;
    if (this.z < 1) {
      this.z = width;
      this.x = random(-width, width);
      this.y = random(-height, height);
      this.pz = this.z;
    }
  }

  show() {
    fill(255);
    noStroke();

    let sx = map(this.x / this.z, 0, 10, 80, width);
    let sy = map(this.y / this.z, 0, 10, 80, height);

    let r = map(this.z, 0, width, 16, 0);

  image(message2,sx,sy,r,r)


  let px = map(this.x / this.pz, 0, 1, 0, width);
      let py = map(this.y / this.pz, 0, 1, 0, height);

      this.pz = this.z;

      stroke(0, 59, 0, random(2,20));
      line(px, py, sx, sy);

  }
}
