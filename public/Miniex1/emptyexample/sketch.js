let banana;
let img;


function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight,WEBGL);
  background(229,69,153);

}

function preload() {
  banana = loadModel('Assets/banana.obj', true)
  img = loadImage('Assets/banana.jpg');

}
function windowResized() {
  resizeCanvas(windowWidth,windowHeight)
}

function draw() {
  // put drawing code here
 background(229,69,153);

//
  ambientLight(150,203,41);
  ambientMaterial(250);
  let dirX = (mouseX / width - 0.5) * 2;
  let dirY = (mouseY / height - 0.5) * 2;
  directionalLight(80, 80, 80, -dirX, -dirY, -0.2);
  //rotateY(millis() / 1000);
  //rotateX(millis() / 459);



// model tags
  texture(img);
  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
  push()
  translate(320,240,0)
  model(banana)
  pop()


}
