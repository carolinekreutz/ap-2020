class Button {
  constructor(x, y, sizeWidth, sizeHeight, img) {
    this.x = x;
    this.y = y;
    this.sizeWidth = sizeWidth;
    this.sizeHeight = sizeHeight;
    this.img = img;
  }

  display() {
    if (((mouseX > this.x-this.sizeWidth/2) && (mouseX < this.x + this.sizeWidth/2)
     && (mouseY > this.y-this.sizeHeight/2) && (mouseY < this.y + this.sizeHeight/2)) &&
       mouseIsPressed){
      image(this.img, this.x, this.y, this.sizeWidth*1.1, this.sizeHeight*1.1);
    }
    else {
      image(this.img, this.x, this.y, this.sizeWidth, this.sizeHeight);
    }
  }
}
