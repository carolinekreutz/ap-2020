function setup() {
  // put setup code here
  createCanvas(640,480, WEBGL);
}

function draw() {
  // put drawing code here
  background(90,255,0);
  ambientLight(150,203,41);
  ambientMaterial(250);
  let dirX = (mouseX / width - 0.5) * 2;
  let dirY = (mouseY / height - 0.5) * 2;
  directionalLight(80, 80, 80, -dirX, -dirY, -0.2);
  rotateY(millis() / 1000);
  rotateX(millis() / 459);
  normalMaterial()

  torus(100,50);


}
