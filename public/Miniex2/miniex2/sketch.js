// "The term "leet" is derived from the word elite, used as an adjective to describe formidable prowess or accomplishment, especially in the fields of online gaming and computer hacking. The leet lexicon includes spellings of the word as 1337 or l33t."
let l33t;
function preload() {
  l33t = loadFont('Assets/leet.ttf');
}

function setup() {
  createCanvas(1900,1800)

}

function draw(){
  //Static shapes
  background('#000000')
  fill('#ffffff');
  textFont(l33t);
  textSize(240);
  textFont(l33t);
  beginShape();
  text("*",300,300);
  endShape();
  beginShape();
  text("*",150,300);
  endShape();

   if (mouseIsPressed) {
     //rotate happy
     translate(215,110);
     rotate(HALF_PI);
   } else {
     //rotate sad
     translate(320,300);
     rotate(PI*radians(86));
   }
  text(")",50,50);
  endShape();
  }
