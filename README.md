# Ap 2020
This is my mini excersises for the AP 2020 course

# **Miniexes**
Here are the links to each respective mini excercise  
[Miniex1](https://gitlab.com/mathiesdamm/ap-2020/-/tree/master/public/Miniex1)
[Miniex2](https://gitlab.com/mathiesdamm/ap-2020/-/tree/master/public/Miniex2)
[Miniex3](https://gitlab.com/mathiesdamm/ap-2020/-/tree/master/public/Miniex3)
[Miniex4](https://gitlab.com/mathiesdamm/ap-2020/-/tree/master/public%2FMiniex4%2Fminiex4)
[Miniex5](https://gitlab.com/mathiesdamm/ap-2020/-/blob/master/public/Miniex5/p5/miniex5)
[Miniex6](https://gitlab.com/mathiesdamm/ap-2020/-/blob/master/public/Miniex6/miniex6)
[Miniex7](https://gitlab.com/mathiesdamm/ap-2020/-/blob/master/public/Miniex7)
[Miniex8](https://gitlab.com/mathiesdamm/ap-2020/-/blob/master/public/Miniex8)


# miniex1
<img alt="YAP" src="https://i.imgur.com/HprzaFh.png">
[RunMe](https://mathiesdamm.gitlab.io/ap-2020/Miniex1/emptyexample/)  

**Information:** The runme couldnt pull the image file fast enough, due to its size  so its a black banana unless run locally - it also takes a while to load depending on the connection and pc.

**Reflection questions:**  
How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
I think the idea of code, starts with reading. You make connection by looking at examples  
You get a sense of how code connects, and how its structure is made.  

How is the coding process different from, or similar to, reading and writing text?
I think writing code is a mix between reading and writing text. Nobody can possibly remember every reference  
and therefore you use a reference, and look at how it has been made by others.  

What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?
I think assigned readings gives us an understanding of the inner workings of code, and gives us a critical view upon code.  
It doesnt necassarily help us in the technical aspect, as i think its mainly "learning by doing".  
But having an understanding of how code affects us, and play a part is a big deal of creating something meaningful.  



# Miniex2
<img alt="YAP" src="https://i.imgur.com/GqxSIQR.png">
[RunMe](https://mathiesdamm.gitlab.io/ap-2020/Miniex2/miniex2/)

**Information:** This has a mousepressed function (it shows the second emoji)

**Reflection questions:** 
1) Describe your program and what you have used and learnt.

My program is a text based emoji, meaning it has no code related to shape functions. Its is only based on text, and therefore instantly changeable to whatever key is needed to be shown.
The colors of the program is delibirately grayscale to minimize cultural identity.

2) How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).
I wanted to create a program inspired by early internet culture. The days of IRC chat rooms and leet speak, the idea being that ones internet identity was interchangeable, and wasnt the subject to judgement.
In current culture, identity rules all - and we have tried to push a close version of our own identity online.

This emoji challenges that in multiple ways, being based solely on text - making it accessible all who can use a keyboard, but at the same time limiting what it can show.
I chose to make a common smiley, but with a boolean trigger for showing a sad and happy emoji, where as to make the emoji happy, you have to constantly be at the computer.
Basically binding you to it.

I took the emoji exercise as a challenge to think about the affects of internet culture, and how it has changed throughout my youth.
For young people their internet identity is almost indistinguishable from their real one - with the invention of instagram, facebook and others.

The problem with brining a physical identity to data, is that you are subject to prejudice.

**References:**

Font used : https://www.dafont.com/leetspeak.font







# Miniex3
<img alt="YAP" src="https://i.imgur.com/ddrbrQW.png">
[RunMe](https://mathiesdamm.gitlab.io/ap-2020/Miniex3/miniex3/)

**Information:**

**Reflection questions:** 
Describe your throbber design, both conceptually and technically.

What is your sketch? What do you want to explore and/or express?
My sketch is throbber, its meant to explore the simplicity while still following the idea of existing.



What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?
The rotation is gated by math, not by a function like millis
I choose it to give the arc a start function and ending easier.
It was simply a choice of giving a function rotation by math rather than time related syntax.

How is time being constructed in computation (refer to both the reading materials and your process of coding)?
The idea of things existing only if they can be stopped challenges the thought of a throbber
The concept of a throbber not having a time gated limit, and simply being a projection of what we think is the server working.
The slowmoving and the fast moving rotation challenges itself, in the idea of is giving the idea of 2 different ending, where there is none

Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?
I think to some extent throbber provides a thing we feel we need but it has no actual use.
It is a symbol of loading information, but why do we need a symbol of that - it provides no information compared a load bar. 
But as computers have come to make GUI's for basically all interaction, alot of people forget about the inner workings and needs something to give an idea of what is happening.



**References:**
Math used inspired by https://editor.p5js.org/harolalmeida/sketches

# Miniex4
<img alt="YAP" src="https://i.imgur.com/AaZHhuA.png">
[RunME](https://mathiesdamm.gitlab.io/ap-2020/Miniex4/miniex4/)

  
**Error 404**

As internet culture continues to be a part of the Hong Kong liberation fight, people are becoming customed to the ever growing threat of facial recognition.
People have resorted to using laser, masks and even wrapping their travel card in tinfoil.
Error 404 attempt to describe a future where faces are blurred out. A place where your online identity is once shrouded.
Much like in the days of IRC chatrooms. We have been taught that facial recognition system are a daily thing, from when we log into facebook and we have automatically been tagged.
Or when you see that a thief have been caught by a camera several kilometers from the crime.
But what happens when that data is misused, and are we ever giving consent to this.
In terms of syntax, I used the CLM tracking facial recognition libary for JS.
I used filters to get the effect of the image that i wanted - it was meant to be non static.
Each of the images is given the position of each eye and the middle of the mouth
image(img,positions[60][0]-100,positions[60][1]-80, 200, 115)
image(img,positions[29][0]-100,positions[29][1]-80, 200, 115)
image(img,positions[24][0]-100,positions[24][1]-80, 200, 115)
The -100 is the offset the image into the correct position rather than the corner being place at the position.

This libary/face reg proves some implications. The libary is not very functional in dark areas, and even has trouble distingusing facial placements for a light skinned person.
Therefore unfortunately it has a hard time finding anchor points for darker skinned people.

Please activate camera when opening the site.
# Miniex5
<img alt="YAP" src="https://i.imgur.com/Z7h6dSr.png">
[RunMe](https://mathiesdamm.gitlab.io/ap-2020/Miniex5/p5/miniex5/)

**DOESITRAININAARHUS**

As having some time to relax and given time to reflect about the materials we have gone through, I decided to take on what I would call an informations system.
I like the idea of creating essentially functionally things, that challenge traditional process. For my example I created what actually is a fully functional weather app.
It is inspired by the "ERDETFREDAG.dk" from the earlier 2000's, but in my case this gives you the info where the weathercast defines that is raining currently.
It is really sensitive in terms of what it will says yes to, as a little rain will maybe be called "cloudy" by the weathercast.
You could create an array with all the values of what could be considered rain, and used that in the if switch.

Info: It says no most of the time, that is due to the different that states that could be considered rain.

I think the depature point was from my inspiration of earlier code. The early 2000's have always seemed like the golden days in terms of code.
So from my making of a smiley based on text, I choose to mix that with my wish to create things with a common purpose.
I changed the way I thought about coding, as using two system to interact with each other - in terms of using a systems api.
I think the first sentences explains some of my understanding of Aesthetic Programming. The idea of creating a thought in the user, in my perspective I prefer to create something that could be usefull but really isnt.
What does it mean by programming as a practice, or even as a method for design? 
I think coding, not limited by a specific libary can give endless opportunities, and if you dont mind having spagetti code, it can be a way of quickly making interactive models.




# Miniex6
<img alt="YAP" src="https://i.imgur.com/E67acjp.png">
[RunME](https://mathiesdamm.gitlab.io/ap-2020/Miniex6/miniex6/)

**ALOOKTHROUGHTHENET**

Describe how does your game/game objects work?  
My initial idea, was to create space invaders, but using class construct together with the p5 play libary proved to be beyond what i was capable of.  
I ended up spending most of the week, trying to implement several different methods on how to make it work, but since there is little to no help to get, I found it quite hard.  
P5 play isnt implemented with error messages yet, so most of the time the screen would be white and i would get no errors.  
However I feel I did learn alot from this approach.  

I did want to show something though, so this is more of a work heavily inspired by one of shiffman's challenges.
I would probably consider it more of a cinematic introduction to a game - so the only interaction is mouse relating to speed.  


Describe how you program the objects and their related attributes and methods in your game.  
The constructor uses 3 attributes - x, z, y 
>   this.x = random(-width, width);
    this.y = random(-height, height);
    this.z = random(width);

Which end up defining most of the math behind the movement and speed.  
I would probably prefer to use a specific seed to generate the images, as it seemed to fix some issues i had with the placement infront of the camera.

What are the characteristics of object-oriented programming and the wider implications of abstraction?  
In terms of programming with a object oriented language, the idea of creating a connection between element is very important - and for larger projects it will be basically mandatory to create classes to define what you want and give attributes.  


Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?  
I remember some games, where as you would load in the game would display a form of throbber - it worked in the sense that it would give you a visual that would distract you, till your game was loaded.  
In more recent years, as fast internet speed is a common thing, and ssd's/nvme's increase loading speeds - games try to create not just a visual display, but also a interaction. This is to accomadate the loading of other players.  
In recent games, such as COD WARZONE, this is displayed by a pregame lobby - were users are encourage to practice against each others - till all are loaded in.  
It is different to the game, in the sense that users have different attributes. So for example, in the game you would have the attribute speed = 1, in the "loadgame" it would be speed = 1.2  
You are put in a state where the "loadgame" is way further in progresssion and plays faster than the actual game. I think it gives a sense of abstraction, and pumps the users up to what about to come.



# Miniex7
<img alt="YAP" src="https://gitlab.com/carolinekreutz/ap2020/-/raw/master/public/mx07/screenshot.PNG">
[RunME](https://mathiesdamm.gitlab.io/ap-2020/Miniex7/)
Rezoom the browser if nothing shows up (Usually 3 dots in the top corner - chrome)

**README: Questions to think about**

_What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?_

_What&#39;s the role of rules and processes in your work?_

_Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?_

- Divided into a &quot;table&quot; in which the patterns are formed
- Predefined figures that with which the program &quot;builds&quot; the patterns

- New patterns every time you refresh (random/unique patterns - made with predefined figures)
- The role of the rules: to give structure to our generative art and to make several smaller patterns from the figures (figures are the building blocks of the patterns)

Syntax wise, the program does a single run of function, meaning it doesn&#39;t loop more than a full turn. It will console log, the array of choices it chose for the specific instance of the program.  An example of this will be the output here.

&quot;pos: Object

1.  draw: false
2.  sides: 6
3.  stepsOut: 8
4.  thinStroke: 1
5.  thickStroke: 3
6.  numShapes: 6
7.  angle: 60
8.  singleStep: 375

&quot;

Here we see the attributes that are connected to each iteration of the &quot;block&quot;/figure. But because of draw : false, it won&#39;t render the &quot;block&quot;. If you run it again, you have a chance of it being true.

A different aspect to pseudo randomness vs randomness, it the idea of us giving the program a specific set of rules, which are very clear. And as we implemented Alpha values to the colors, those set of rules changed for what we gave the program.
The alpha values for the colors, helped create layers - where 2 different or maybe the same color is overlaid, giving the program a much bigger set of possibilities.

The program does have a weight system, so as of currently the program only create a relationship of control between program and coder. Not between user and program. A way to implement this would be to make a slider for the weight system, so the user has a form of determining the output, though still heavily affected by randomness.



A lot of the inspiration comes from a video series by Matthew Epler

Source : [https://www.youtube.com/watch?v=C8DeS2i81IY](https://www.youtube.com/watch?v=C8DeS2i81IY)

Program was made together with [Caroline Kreutz](https://gitlab.com/carolinekreutz)
# Miniex8
<img alt="YAP" src="https://i.imgur.com/YfpFKdk.png">
[RunME](https://mathiesdamm.gitlab.io/ap-2020/Miniex8/)

**Mad libs & mad sicks**

We were inspired by the circumstances in the world, and how we ourselves are affected by the Corona virus crisis. We wanted to base our work on this, however we did not want it to be as dark and negative as it is in reality. We therefore decided to create a form of distraction while the link to the Corona virus is still apparent in the program. When we brainstorming ideas, we thought of the Mad Libs game where you place words in the blank spaces of a story. We thought it would be funny to implement this sort of idea in the program - that way, the user will always get something different from the program, and perhaps even a laugh. To implement this idea, we decided to display a poem called Alone with everybody by Charles Bukowski, which is a very sad poem. That is why we removed the negative words and made sure only positive words could fill the blank spaces. To connect this idea to the Corona virus, every time the number of infected goes up, the selected words in the poem change. 
We found the concept of vocable code and code poetry very difficult. As we are just learning how to code, we have mostly focused on the functional and technical aspects so far. It was therefore a big challenge to decide how to make the code itself poetic - and not just the execution of the code. The chapter “vocable code” mentions how language experiments with unorthodox combinations - which is what we attempted to do by taking a rather dark poem and combining it with Corona virus numbers - using code. We are therefore sort of poetic in the sense that we consciously combine very different elements to send a certain message with code. 
In the text “The aesthetics of generative code”, it is mentioned how generative code has poetic qualities as it does not operate in a single moment in time and space. Our program is not a series of consecutive actions either - the outcome is always different even if the process is the same. It was important for us to achieve the generative and aesthetic aspect in our program to create something different and fun. 
While our program might not be the most groundbreaking or creative program, we did the best we could with the skills we have acquired throughout this course to create something special. 
There are no voices in our program. When we came up with ideas for what to create, it did not occur to us how we could implement voices. And as we look at the finished product, we don’t believe voices are necessary for our program, even though they can generally contribute to the aesthetic aspects of code. For our program though, the voices in the users’ heads as they read the different variations of the poem are enough.  
However, the performative aspect of code is quite relevant to our program. Ours is performative as the poem changes along with the meaning of it - and it might not even make sense if the words do not make sense together. However, this is not important - we believe it is still performative if it sparks a reaction, and hopefully a positive one. According to speech act theorist, John Langshaw Austin, a successful performative utterance is defined by it triggering a certain chain of effects. It is difficult to say if our program is performative then - we cannot be sure it will trigger a certain chain of effects. But perhaps at some level, it triggers feelings, thoughts, or reactions. 

In terms of the analysis of the code syntax, we chose to use an external library to help create the seed for the random function, well aware that p5 have already implemented a seed function, but it basically just follows a specific pattern, and is not reliable for what we wanted to create. At the same the seed we had, required us to create of variables for the different output. Because we wanted to create the same output on each load, but still change on infected numbers.
We used an api (in json form), and took the data about how many infected with covid 19 there currently are in Denmark, and used that number to create a seed. So that every time the program updates, it fetches data from the api and changes the input to the seed.
In this way we could display technical ability, while still challenging us to use a different method of creating randomness.
For the syntax for variables, we chose to pick a set of positive words and use them instead of more formal format - as to add an effect to the performative program.

original poem : Alone with everybody by Charles Bukowski

Project was done in collab with [Amanda Hansen](https://pad.riseup.net/redirect#https%3A//gitlab.com/amanda.hansen1404)

# Miniex9
[](Linktoscreenshot)
[](linktorunme)
# Miniex10
[](Linktoscreenshot)
[](linktorunme)